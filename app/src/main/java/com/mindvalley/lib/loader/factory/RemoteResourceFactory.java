package com.mindvalley.lib.loader.factory;

import com.mindvalley.lib.loader.factory.model.ImageResource;
import com.mindvalley.lib.loader.factory.model.RemoteResource;
import com.mindvalley.lib.loader.factory.model.ByteResource;
import com.mindvalley.lib.loader.factory.model.TextResource;

/**
 * Created by souvik on 7/11/2016.
 */
public final class RemoteResourceFactory {

    private RemoteResourceFactory() {

    }

    public static final RemoteResource createRemoteResource(String mime) {
        int type = 0;

        if (mime.equals("application/json")) {
            type = RemoteResource.TYPE_TEXT;
        } else if (mime.startsWith("image")) {
            type = RemoteResource.TYPE_IMAGE;
        }

        return createRemoteResource(type);
    }

    public static final RemoteResource createRemoteResource(final int type) {
        RemoteResource remoteResource = null;

        switch (type) {
            case RemoteResource.TYPE_IMAGE:
                remoteResource = new ImageResource();
                break;
            case RemoteResource.TYPE_TEXT:
                remoteResource = new TextResource();
                break;
            default:
                remoteResource = new ByteResource();
                break;
        }

        return remoteResource;
    }

}
