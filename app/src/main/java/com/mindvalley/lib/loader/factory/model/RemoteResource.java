package com.mindvalley.lib.loader.factory.model;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by souvik on 7/10/2016.
 */
public interface RemoteResource<ResourceType> {

    public static final int TYPE_TEXT = 1;
    public static final int TYPE_IMAGE = 2;
    public static final int TYPE_STREAM = 3;

    public void prepare(InputStream inputStream) throws IOException;
    public ResourceType getResource();
    public int size();

}
